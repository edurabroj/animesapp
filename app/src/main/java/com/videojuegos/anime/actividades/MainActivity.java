package com.videojuegos.anime.actividades;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.videojuegos.anime.R;
import com.videojuegos.anime.adaptadores.AnimeAdaptador;
import com.videojuegos.anime.entidades.Anime;
import com.videojuegos.anime.servicio.IServicio;
import com.videojuegos.anime.servicio.ServicioProveedor;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private IServicio servicio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        servicio = ServicioProveedor.getServicio();
        ListView listView = (ListView) findViewById(R.id.listView);
        Button btnTodos = (Button) findViewById(R.id.btnTodos);
        Button btnFavoritos = (Button) findViewById(R.id.btnFavoritos);

        final AnimeAdaptador adaptador = new AnimeAdaptador(MainActivity.this,
                                                        R.layout.item,
                                                        new ArrayList<Anime>());
        listView.setAdapter(adaptador);


        assert btnTodos != null;
        btnTodos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                servicio.getTodos().enqueue(new Callback<List<Anime>>() {
                    @Override
                    public void onResponse(Call<List<Anime>> call, Response<List<Anime>> response) {
                        if(response.isSuccessful()){
                            Toast.makeText(MainActivity.this, "TODOS",Toast.LENGTH_SHORT ).show();
                            List<Anime> datos = response.body();
                            adaptador.clear();
                            adaptador.addAll(datos);
                        }else{
                            Toast.makeText(MainActivity.this, "Error en la respuesta: " + response.message(),Toast.LENGTH_LONG ).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Anime>> call, Throwable t) {
                        Toast.makeText(MainActivity.this, "Fallo: " + t.getMessage(),Toast.LENGTH_LONG ).show();
                    }
                });

            }
        });

        assert btnFavoritos != null;
        btnFavoritos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                servicio.getFavoritos("710912").enqueue(new Callback<List<Anime>>() {
                    @Override
                    public void onResponse(Call<List<Anime>> call, Response<List<Anime>> response) {
                        if(response.isSuccessful()){
                            Toast.makeText(MainActivity.this, "FAVORITOS",Toast.LENGTH_SHORT ).show();
                            List<Anime> datos = response.body();
                            adaptador.clear();
                            adaptador.addAll(datos);
                        }else{
                            Toast.makeText(MainActivity.this, "Error en la respuesta: " + response.message(),Toast.LENGTH_LONG ).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Anime>> call, Throwable t) {
                        Toast.makeText(MainActivity.this, "Fallo: " + t.getMessage(),Toast.LENGTH_LONG ).show();
                    }
                });
            }
        });


    }
}

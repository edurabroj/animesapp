package com.videojuegos.anime.adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.videojuegos.anime.R;
import com.videojuegos.anime.entidades.Anime;
import com.videojuegos.anime.servicio.IServicio;
import com.videojuegos.anime.servicio.ServicioProveedor;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by USER on 10/12/2017.
 */
public class AnimeAdaptador extends ArrayAdapter<Anime> {
    private IServicio servicio;

    public AnimeAdaptador(Context context, int resource, List<Anime> objects) {
        super(context, resource, objects);
        servicio = ServicioProveedor.getServicio();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Anime anime = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item, parent, false);
        }
        ImageView imagen = (ImageView) convertView.findViewById(R.id.imagen);
        Glide.with(getContext())
                .load(anime.getImagen())
                .into(imagen);

        TextView titulo = (TextView) convertView.findViewById(R.id.titulo);
        titulo.setText(anime.getNombre());

        TextView descripcion = (TextView) convertView.findViewById(R.id.descripcion);
        descripcion.setText(anime.getDescripcion());

        ImageButton btnSetFavorite = (ImageButton) convertView.findViewById(R.id.btnSetFavorite);
        btnSetFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                servicio.setFavorito("710912",anime.getId()).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if(response.isSuccessful()){
                            Toast.makeText(getContext(),"LISTO",Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getContext(),"ERROR DE SERVIDOR: " + response.message() ,Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(getContext(),"ERROR DE RESPUESTA: " + t.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        return convertView;
    }
}

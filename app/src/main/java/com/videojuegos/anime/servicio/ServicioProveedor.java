package com.videojuegos.anime.servicio;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 10/12/2017.
 */
public class ServicioProveedor {
    private static IServicio servicio;

    public static IServicio getServicio(){
        if(servicio==null){
            servicio = new Retrofit.Builder()
                    .baseUrl("http://upn.lumenagile.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(IServicio.class);
        }
        return servicio;
    }
}

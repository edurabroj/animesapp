package com.videojuegos.anime.servicio;

import com.videojuegos.anime.entidades.Anime;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by USER on 10/12/2017.
 */
public interface IServicio {
    @GET("animes")
    Call<List<Anime>> getTodos();

    @GET("{codigo}/animes")
    Call<List<Anime>> getFavoritos(@Path("codigo") String codigo);

    @FormUrlEncoded
    @POST("{codigo}/anime/favorite")
    Call<ResponseBody> setFavorito
    (
        @Path("codigo") String codigo,
        @Field("id") int id
    );
}
